import numpy as np
import xarray as xr
import xesmf as xe
import sys
import os
import glob
import time
import socket
#import platform
import yaml
import datetime
from dataclasses import dataclass
from os import listdir
from os.path import isfile, join
import pprint
from multiprocessing.pool import Pool

@dataclass
class config_data:
	config_file_path : str   
	rank  			 : int	 

	def get_config(self):
		self.config_file      = yaml.safe_load(open(self.config_file_path, "rb"))
		self.ERA_param        = self.config_file.get("ERA_param")
		self.out_deg          = self.config_file.get("out_deg")
		self.pressure_level   = self.config_file.get("pressure_level")
		self.in_dir_prefix    = self.config_file.get("in_dir_prefix")
		self.out_file_prefix  = self.config_file.get("out_file_prefix")
		self.algorithm        = self.config_file.get("algorithm")
		try:
			self.year         = self.config_file.get("year")[self.rank]
		except IndexError:
			print ("Year index for rank NOT found, rank:", self.rank)
			self.year 		  = -1

		self.in_dir   = self.in_dir_prefix  + "/"+str(self.year) #+"/*"
		self.out_file = self.out_file_prefix+ "_"+str(self.year)+"_"+str(self.out_deg)+"deg.nc"

		return self	
	#==============================================================#
	# Format #
	def print_celing_a(self):
		print("\u2554","\u2550"*150, "\u2557", sep="")
		print("\u2551 \u2554","\u2550"*146, "\u2557 \u2551", sep="")
	def print_heading_a(self, prompt ):
		print("\u2551 \u2551{0:^146s}\u2551 \u2551".format( prompt ))
	def print_format_a(self, prompt, variable ):
		print("\u2551 \u2551{0:>20s} \u25B6 {1:<123s}\u2551 \u2551".format(prompt, str(variable)))	
	def print_floor_a(self):
		print("\u2551 \u255A","\u2550"*146, "\u255D \u2551", sep="")
		print("\u255A","\u2550"*150, "\u255D", sep="")	

	def	print_celing_b(self):
		print ("\u250C","\u2500"*150, "\u2510", sep="") 
	def print_date_b(self):
		print("\u250A{0:^150s}\u250A".format( self.start.strftime("%c") ))	
	def print_format_b(self, prompt, variable ):
		print("\u250A{0:>12s} \u25BB {1:<135s}\u250A".format(prompt, str(variable) ))
	def print_error_b (self, prompt):
		print (" \u2590\u2590\u2590\u2590\u2590 {0:^138s} \u2590\u2590\u2590\u2590\u2590\n".format(prompt))	
	def print_floor_b(self):
		print ("\u2514","\u2500"*150, "\u2518", sep="")	
	#==============================================================# 
	def print_common_param(self):
		self.print_celing_a()
		self.print_heading_a( "Common Param" )
		self.print_format_a(  "ERA_param"       , era_config_data.ERA_param )
		self.print_format_a(  "config_file_path", era_config_data.config_file_path )
		self.print_format_a(  "out_deg"  , era_config_data.out_deg )
		self.print_format_a(  "algorithm", era_config_data.algorithm )
		self.print_format_a(  "pressure_level" , era_config_data.pressure_level )
		self.print_format_a(  "in_dir_prefix"  , era_config_data.in_dir_prefix )
		self.print_format_a(  "out_file_prefix", era_config_data.out_file_prefix )
		self.print_floor_a()

	def print_specific_param(self):
		self.start = datetime.datetime.now()
		self.print_celing_b()
		self.print_date_b()
		self.print_format_b( "rank",     self.rank )
		self.print_format_b( "node",     socket.gethostname() )
		self.print_format_b( "year",     self.year)
		self.print_format_b( "in_dir",   self.in_dir)
		self.print_format_b( "out_file", self.out_file)

		if self.year != -1:
			self.inspect_data = xr.open_mfdataset(self.in_dir +"/*")  
			try:
				self.print_format_b( "Title:", self.inspect_data.attrs["title"] )
			except Exception as e:
				print ('Title can not be printed:', e)	
			self.print_format_b( "Dimension:", '{}'.format(dict(self.inspect_data.dims)) )
			self.print_format_b( "Variables:", '{}'.format(list(self.inspect_data.keys())))
			self.inspect_data.close()
		else:
			print()
			self.print_error_b ("Not a valid year: regrid function will not be called")
		self.print_floor_b()

	def print_end (self):
		self.end = datetime.datetime.now() 
		print("[{0:<4s}{1:<3s}{2:>8s}] ".format(self.end.strftime("%a"), self.end.strftime("%d"), self.end.strftime("%X")), end="")
		print("{0:>10s} {1:<4s} {2:>8s} {3:<80s}".format("End rank:", str(self.rank)+",", self.status, self.out_file), end="")
		diff = (self.end - self.start)	
		seconds_in_day = 24 * 60 * 60
		diff_m_s = divmod(diff.days * seconds_in_day + diff.seconds, 60)
		print(" {0:<2s} ({1:>4s}m, {2:>2s}s)\n".format("", str(diff_m_s[0]), str(diff_m_s[1]) ) ) 



era_config_data = config_data(sys.argv[1], int(sys.argv[2]))
era_config_data.get_config()

era_config_data.print_common_param()
print("\n")
era_config_data.print_specific_param()
print("\n")

if os.path.exists(era_config_data.out_file):
	print ("Destination file already exists:", era_config_data.out_file)
	print ("Exit\n")
	exit(1)
if  era_config_data.year == -1:
	print ("Invalid year:", era_config_data.year)
	print ("Exit\n")
	exit(1)
 
era_config_data.status = "Starting ..." 

def regrid_func (in_file, ddeg_out, algorithm, p_level = None ):

	data = xr.open_dataset(in_file, chunks = {"time":20})
	if p_level:
		ds_in = data.sel(level = p_level) 
	else: 
		ds_in = data
		
	#print ("Forced return (Testing only)\n"); time.sleep(10); return #time.sleep(85); return 	
	if 'latitude' in ds_in.coords:
        	ds_in = ds_in.rename({'latitude': 'lat', 'longitude': 'lon'})  

	grid_out = xr.Dataset(
	{
		'lat': (['lat'], np.arange(-90+ddeg_out/2, 90, ddeg_out)),
		'lon': (['lon'], np.arange(0, 360, ddeg_out)),
	})
	regridder = xe.Regridder(ds_in, grid_out, algorithm, periodic=True )  
	ds_out = regridder(ds_in)
	ds_out = ds_out.compute()
	#ds_out.to_netcdf(out_file, compute=True)	
	return ds_out    ## ds_out.compute() -> does not work

if __name__ == "__main__":

	with Pool() as pool:
		all_files_param = [(join(era_config_data.in_dir, f),  float(era_config_data.out_deg) , era_config_data.algorithm, None ) 
							for f in listdir(era_config_data.in_dir) if isfile(join(era_config_data.in_dir, f))]
		pprint.pprint(all_files_param)
		
		#for result in pool.starmap(regrid_func , all_files_param):
			#print(f'Got result: {result}', flush=True)
		result = pool.starmap(regrid_func , all_files_param)
	
	era_config_data.status = "End" 
	ds = xr.merge([r for r in result ])
	ds.to_netcdf( era_config_data.out_file, compute=True )
	#print(ds)

	era_config_data.print_end()


