import cdsapi
import sys
import os

month_dates = [ ['0101', '0131'], ['0201', '0228'], ['0301', '0331'], ['0401', '0430'],
                ['0501', '0531'], ['0601', '0630'], ['0701', '0731'], ['0801', '0831'],
                ['0901', '0930'], ['1001', '1031'], ['1101', '1130'], ['1201', '1231'] ]

c = cdsapi.Client()

def get_data_param( var_name, year, month):   
    return {
        'product_type': 'reanalysis',
        'variable': var_name,
        'year':  [ year ],
        'month': [ month ],
        'day': [
            '01', '02', '03', '04', '05', '06', '07', '08', '09',
            '10', '11', '12', '13', '14', '15', '16', '17', '18',
            '19', '20', '21', '22', '23', '24', '25', '26', '27',
            '28', '29', '30', '31',
        ],
        'time': [
            '00:00', '01:00', '02:00', '03:00', '04:00', '05:00',
            '06:00', '07:00', '08:00', '09:00', '10:00', '11:00',
            '12:00', '13:00', '14:00', '15:00', '16:00', '17:00',
            '18:00', '19:00', '20:00', '21:00', '22:00', '23:00',
        ],
        'area': [ 90, -180, -90, 180,],
        'format': 'netcdf',
        'grid': [0.25, 0.25],
    }

def download_data(data_param, file_path):
    c.retrieve(
        'reanalysis-era5-single-levels',
        data_param,
        file_path)

year = sys.argv[1]
print (year)

dir_path = '/g/data/wb00/admin/testing/cds_era5/single-levels/reanalysis/tisr/'+year 
print (dir_path)
if not os.path.exists(dir_path):
    os.makedirs (dir_path)

# 01
file_path = dir_path + '/tisr_era5_oper_sfc_'+year+month_dates[0][0]+'-'+year+month_dates[0][1] + '.nc'
print(file_path)
data_param = get_data_param('toa_incident_solar_radiation', year, '01')
download_data(data_param, file_path)
# 02
file_path = dir_path + '/tisr_era5_oper_sfc_'+year+month_dates[1][0]+'-'+year+month_dates[1][1] + '.nc'
print(file_path)
data_param = get_data_param('toa_incident_solar_radiation', year, '02')
download_data(data_param, file_path)
# 03
file_path = dir_path + '/tisr_era5_oper_sfc_'+year+month_dates[2][0]+'-'+year+month_dates[2][1] + '.nc'
print(file_path)
data_param = get_data_param('toa_incident_solar_radiation', year, '03')
download_data(data_param, file_path)
# 04
file_path = dir_path + '/tisr_era5_oper_sfc_'+year+month_dates[3][0]+'-'+year+month_dates[3][1] + '.nc'
print(file_path)
data_param = get_data_param('toa_incident_solar_radiation', year, '04')
download_data(data_param, file_path) 
# 05
file_path = dir_path + '/tisr_era5_oper_sfc_'+year+month_dates[4][0]+'-'+year+month_dates[4][1] + '.nc'
print(file_path)
data_param = get_data_param('toa_incident_solar_radiation', year, '05')
download_data(data_param, file_path)
# 06
file_path = dir_path + '/tisr_era5_oper_sfc_'+year+month_dates[5][0]+'-'+year+month_dates[5][1] + '.nc'
print(file_path)
data_param = get_data_param('toa_incident_solar_radiation', year, '06')
download_data(data_param, file_path)
# 07
file_path = dir_path + '/tisr_era5_oper_sfc_'+year+month_dates[6][0]+'-'+year+month_dates[6][1] + '.nc'
print(file_path)
data_param = get_data_param('toa_incident_solar_radiation', year, '07')
download_data(data_param, file_path)
# 08
file_path = dir_path + '/tisr_era5_oper_sfc_'+year+month_dates[7][0]+'-'+year+month_dates[7][1] + '.nc'
print(file_path)
data_param = get_data_param('toa_incident_solar_radiation', year, '08')
download_data(data_param, file_path)
# 09
file_path = dir_path + '/tisr_era5_oper_sfc_'+year+month_dates[8][0]+'-'+year+month_dates[8][1] + '.nc'
print(file_path)
data_param = get_data_param('toa_incident_solar_radiation', year, '09')
download_data(data_param, file_path)
# 10
file_path = dir_path + '/tisr_era5_oper_sfc_'+year+month_dates[9][0]+'-'+year+month_dates[9][1] + '.nc'
print(file_path)
data_param = get_data_param('toa_incident_solar_radiation', year, '10')
download_data(data_param, file_path)
# 11
file_path = dir_path + '/tisr_era5_oper_sfc_'+year+month_dates[10][0]+'-'+year+month_dates[10][1] + '.nc'
print(file_path)
data_param = get_data_param('toa_incident_solar_radiation', year, '11')
download_data(data_param, file_path)
# 12
file_path = dir_path + '/tisr_era5_oper_sfc_'+year+month_dates[11][0]+'-'+year+month_dates[11][1] + '.nc'
print(file_path)
data_param = get_data_param('toa_incident_solar_radiation', year, '12')
download_data(data_param, file_path)

 