#!/bin/bash
# Help the MPI to run python script in separated conda envs.

. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/weatherbench

HOST=`hostname`
RANK=`cat $PBS_NODEFILE | uniq | grep -n $HOST | cut -f1 -d :` 
export NODE_RANK=$((RANK-1)) 
sleep $((${NODE_RANK}*1))
 
START=$((NODE_RANK*WR_PER_NODE)) 
END=$((((NODE_RANK+1)*WR_PER_NODE)-1)) 
echo "NODE: ${NODE_RANK},  START: ${START},  END: ${END}"

for (( c=$START; c<=$END; c++ ));do
    python "${REGRID_SCRIPT}" "${CONFIG_FILE}" ${c}  &
    JOBS=$(jobs | wc -l)
    JOBS=$((${JOBS}-1))   
#    if [ "${JOBS}" -ge "${WR_PER_NODE}" ]; then J_FLAG='o'
#    #echo -e "\t\t\t\t\t\t\t ( Total jobs: ${JOBS}: over)"
#    else   J_FLAG='u'
#    #echo -e "\t\t\t\t\t\t\t ( Total jobs: ${JOBS}: Under )"    
#    fi
    printf 'Rank: %-5d Host: %s ( local task: %-2d %s) \n' "${c}" "`hostname`" "${JOBS}" "${J_FLAG}"
done 
wait    