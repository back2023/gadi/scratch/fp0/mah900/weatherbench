import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import numpy as np
import xarray as xr
import xesmf as xe
import dask.array as da
import sys
import os
import glob
import time
import socket
import platform
import yaml
from dask.diagnostics import ProgressBar
import datetime
from dask.distributed import Client, progress


def add_2d(ds):
    ds['lat2d'] = ds.lat.expand_dims({'lon': ds.lon}).transpose()
    ds['lon2d'] = ds.lon.expand_dims({'lat': ds.lat})
    return ds

def convert_z_to_orography(ds):
    ds['z'] = ds.z / 9.80665
    ds = ds.rename({'z': 'orography'})
    ds.orography.attrs['units'] = 'm'
    return ds


def regrid_func_constant (ds_in, ddeg_out, algorithm ):
	grid_out = xr.Dataset(
	{
		'lat': (['lat'], np.arange(-90+ddeg_out/2, 90, ddeg_out)),
		'lon': (['lon'], np.arange(0, 360, ddeg_out)),
	})
	regridder = xe.Regridder(ds_in, grid_out, algorithm, periodic=True )  
	ds_out = regridder(ds_in)
	#ds_out.to_netcdf(out_file, compute=True)
	return ds_out

start = datetime.datetime.now()
algorithm = "bilinear"
ddeg_out = 5.625 


orography_in  = "/g/data/rt52/era5/single-levels/reanalysis/z/1979/z_era5_oper_sfc_19790101-19790131.nc"
orography_data = xr.open_dataset(orography_in, chunks = {"time":1})
orography_data = orography_data.isel(time=0).drop('time')   
#orography_data = orography_data.rename({'z': 'orography'}) 

land_binary_mask_in  = "/g/data/rt52/era5/single-levels/reanalysis/lsm/1979/lsm_era5_oper_sfc_19790101-19790131.nc"
land_binary_mask_data = xr.open_dataset(land_binary_mask_in, chunks = {"time":1})
land_binary_mask_data = land_binary_mask_data.isel(time=0).drop('time')  

soil_type_in  = "/g/data/rt52/era5/single-levels/reanalysis/slt/1979/slt_era5_oper_sfc_19790101-19790131.nc"
soil_type_data = xr.open_dataset(soil_type_in, chunks = {"time":1})
soil_type_data = soil_type_data.isel(time=0).drop('time')  

#constant = xr.Dataset()
constant_data = xr.merge([orography_data, land_binary_mask_data, soil_type_data])
constant_data  = constant_data.rename({'latitude': 'lat', 'longitude': 'lon'}) 
#constant_data.drop('time')   

#orography_out = os.path.join(out_dir, "orography")
ds_out = regrid_func_constant (constant_data , ddeg_out, algorithm )
ds_out = convert_z_to_orography(add_2d(ds_out)) 

out_file = "/g/data/wb00/admin/testing/NCI_weatherbench/5.625deg/constants.nc"
#if not os.path.exists(out_file):
#	os.mkdir (out_file)
ds_out.to_netcdf(out_file, compute=True)

end = datetime.datetime.now() 
diff = (end - start)	
seconds_in_day = 24 * 60 * 60
diff_m_s = divmod(diff.days * seconds_in_day + diff.seconds, 60)
print(" {0:<2s} ({1:>4s}m, {2:>2s}s)\n".format("", str(diff_m_s[0]), str(diff_m_s[1]) ) ) 
