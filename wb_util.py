import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import numpy as np
import xarray as xr
import xesmf as xe
import dask.array as da
import glob
import matplotlib as mpl
import pprint 
from multiprocessing.pool import Pool
from multiprocessing import set_start_method

def rmse_a_year (y, file1, file2, p_id):
    ds_1 = xr.open_dataset(file1)
    ds_2 = xr.open_dataset(file2)
    try:    
        rmse = ( ( (ds_1[p_id] - ds_2[p_id])**2).sum() )/len(ds_1[p_id] )
        rmse  = rmse**(1/2) 
        print ( str(y) + ", ", end="", flush = True)
    except ValueError:
        print( str(y) + " (Error), ", end = "", flush = True)
        rmse = -0.1
    ds_1.close()
    ds_2.close()  
    return y, rmse
    
def yearly_rmse_mp (wb_dir, NCI_dir, p_name, p_id):
    
    all_params = []
    #for year in range (1979,2019):
    for year in range (1979,1980):
        file1 = wb_dir  + '/'+p_name+'/'+p_name+'_'+ str(year) + '_5.625deg.nc'
        file2 = NCI_dir + '/'+p_name+'/'+p_name+'_'+ str(year) + '_5.625deg.nc'
        all_params.append ( (year, file1, file2, p_id) )
        
    #pprint.pprint(all_params)    
    with Pool() as pool:                  
        year_list, rmse_list  = zip(* pool.starmap(rmse_a_year , all_params) )               
    return year_list, rmse_list 