import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import numpy as np
import xarray as xr
import xesmf as xe
import dask.array as da
import sys
import os
import glob
import time
import socket
import platform
import yaml
from dask.diagnostics import ProgressBar
import datetime
from dask.distributed import Client, progress
 
out_deg = "" 
orography_file = ""
land_sea_mask  = ""
soil_type      = ""


in_dir   = in_dir_prefix +"/"+str(year)+"/*"
out_file = out_file_prefix+"_"+str(year)+"_"+str(out_deg)+"deg.nc"

 
start = datetime.datetime.now()

 
 
def regrid_func_single (in_dir, out_file, ddeg_out, algorithm ):

	data = xr.open_mfdataset(in_dir, chunks = {"time":1}, parallel=False)
	ds_in = data		
	#print ("Forced return (Testing only)\n"); time.sleep(5); return  
	
	if 'latitude' in ds_in.coords:
        	ds_in = ds_in.rename({'latitude': 'lat', 'longitude': 'lon'})  

	grid_out = xr.Dataset(
	{
		'lat': (['lat'], np.arange(-90+ddeg_out/2, 90, ddeg_out)),
		'lon': (['lon'], np.arange(0, 360, ddeg_out)),
	})
	regridder = xe.Regridder(ds_in, grid_out, algorithm, periodic=True )  
	ds_out = regridder(ds_in)
	ds_out.to_netcdf(out_file, compute=True)

 
#end = datetime.datetime.now() 
#print("[{0:<4s}{1:<3s}{2:>8s}] ".format(end.strftime("%a"), end.strftime("%d"), end.strftime("%X")), end="")
#print("{0:>10s} {1:<4s} {2:>8s} {3:<80s}".format("End rank:", str(rank)+",", status, out_file), end="")
#diff = (end - start)	
#seconds_in_day = 24 * 60 * 60
#diff_m_s = divmod(diff.days * seconds_in_day + diff.seconds, 60)
#print(" {0:<2s} ({1:>4s}m, {2:>2s}s)\n".format("", str(diff_m_s[0]), str(diff_m_s[1]) ) ) 
