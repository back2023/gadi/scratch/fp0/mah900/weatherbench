
import numpy as np
import xarray as xr
import xesmf as xe
import sys
import os
import glob
import time
import socket
import platform
import yaml
import datetime

 
deg_config_file_path   = sys.argv[1] 
param_config_file_path = sys.argv[2]
rank                   = int(sys.argv[3])

deg_config_file  = yaml.safe_load(open(deg_config_file_path, "rb"))
out_deg          = deg_config_file.get("out_deg")
out_file_prefix  = deg_config_file.get("out_file_prefix")
algorithm        = deg_config_file.get("algorithm")  

param_config_file = yaml.safe_load(open(param_config_file_path, "rb"))
ERA_param_id      = param_config_file.get("ERA_param_id")
pressure_level    = param_config_file.get("pressure_level")
in_dir_prefix     = param_config_file.get("in_dir_prefix")
param_name        = param_config_file.get("param_name")
try:
	year          = param_config_file.get("year")[rank]
except IndexError:
	print ("Year index for rank NOT found, rank:", rank)
	year = -1

in_dir    = in_dir_prefix +"/"+str(year)+"/*"
out_dir   = os.path.join( out_file_prefix, param_name )
#out_file  = out_dir  +"/"+ param_name  +"_"+str(year)+"_"+str(out_deg)+"deg.nc"
out_file  = os.path.join( out_dir , param_name+"_"+str(year)+"_"+str(out_deg)+"deg.nc" )

time.sleep(5)
if rank != 0:
	time.sleep(3+rank*5)

if (rank==0):
	print(
	  ( "\u2554" + "\u2550"*180 + "\u2557" + "\n" 
		"\u2551 \u2554" + "\u2550"*176 + "\u2557 \u2551" + "\n" 
	    "\u2551 \u2551{0:^176s}\u2551 \u2551".format("Deg Config") + "\n"  
 	    "\u2551 \u2551{0:>20s} \u25B6 {1:<153s}\u2551 \u2551".format("deg_config_file_path", deg_config_file_path) + "\n"
	    "\u2551 \u2551{0:>20s} \u25B6 {1:<153f}\u2551 \u2551".format("out_deg", out_deg) + "\n" 
	    "\u2551 \u2551{0:>20s} \u25B6 {1:<153s}\u2551 \u2551".format("algorithm", algorithm) + "\n" 
	    "\u2551 \u255A" + "\u2550"*176 + "\u255D \u2551" + "\n" 
	    "\u255A" + "\u2550"*180 + "\u255D" ),
	  flush=True 
	)

if (rank==0):
	print(
	  ( "\u2554" + "\u2550"*180 + "\u2557" + "\n" 
	    "\u2551 \u2554" + "\u2550"*176 + "\u2557 \u2551" + "\n" 
	    "\u2551 \u2551{0:^176s}\u2551 \u2551".format("Param Config") + "\n" 
	    "\u2551 \u2551{0:>23s} \u25B6 {1:<150s}\u2551 \u2551".format("ERA_param_id", ERA_param_id) + "\n" 
	    "\u2551 \u2551{0:>23s} \u25B6 {1:<150s}\u2551 \u2551".format("param_name"  , param_name) + "\n" 
	    "\u2551 \u2551{0:>23s} \u25B6 {1:<150s}\u2551 \u2551".format("param_config_file_path", param_config_file_path) + "\n" 
	    "\u2551 \u2551{0:>23s} \u25B6 {1:<150s}\u2551 \u2551".format("pressure_level", str(pressure_level)) + "\n" 
	    "\u2551 \u2551{0:>23s} \u25B6 {1:<150s}\u2551 \u2551".format("in_dir_prefix", in_dir_prefix ) + "\n" 
	    "\u2551 \u2551{0:>23s} \u25B6 {1:<150s}\u2551 \u2551".format("out_dir", out_dir) + "\n" 
	    "\u2551 \u255A" + "\u2550"*176 + "\u255D \u2551" + "\n" 
	    "\u255A" + "\u2550"*180 + "\u255D"  ),
	  flush=True 
	)
	
start = datetime.datetime.now()

print (
  ( "\u250C" + "\u2500"*180 + "\u2510" + "\n" 
    "\u250A{0:^180s}\u250A".format( start.strftime("%c") ) + "\n" 
    "\u250A{0:>12s} \u25BB {1:<165d}\u250A".format("rank", rank) + "\n" 
    "\u250A{0:>12s} \u25BB {1:<165s}\u250A".format("node", socket.gethostname()) + "\n" 
    "\u250A{0:>12s} \u25BB {1:<165d}\u250A".format("year", year) + "\n" 
    "\u250A{0:>12s} \u25BB {1:<165s}\u250A".format("in_dir", in_dir) + "\n" 
    "\u250A{0:>12s} \u25BB {1:<165s}\u250A".format("out_file", out_file)   ),
  flush=True 
)    

if year != -1:
	inspect_data = xr.open_mfdataset(in_dir)  
	title = ""
	try:
		title = inspect_data.attrs["title"] 
	except Exception as e:
		title = "NULL"
	print(
	  ( "\u250A{0:>12s} \u25BB {1:<165s}\u250A".format( "Title:", title ) + "\n" 	
	    "\u250A{0:>12s} \u25BB {1:<165s}\u250A".format( "Dimension:", '{}'.format(dict(inspect_data.dims)) )  + "\n" 
	    "\u250A{0:>12s} \u25BB {1:<165s}\u250A".format( "Variables:", '{}'.format(list(inspect_data.keys())))      ),
	  flush=True 
    )    	    
	inspect_data.close()
else:
	print (" \u2590\u2590\u2590\u2590\u2590 {0:^168s} \u2590\u2590\u2590\u2590\u2590".format("Not a valid year: regrid function will not be invoked"))	
print ("\u2514","\u2500"*180, "\u2518", sep="")
#print("\n")

if not os.path.exists(out_dir):
	os.makedirs(out_dir)

if os.path.exists(out_file):
	print ("Exit: Destination file already exists:", out_file)
	exit(1)
 
def regrid_func (in_dir, out_file, ddeg_out, algorithm, p_level = None ):

	data = xr.open_mfdataset(in_dir, chunks = {"time":1}, parallel = False)
	if p_level:
		ds_in = data.sel(level = p_level) 
	else: 
		ds_in = data	
	#print ("Forced return (Testing only)\n"); time.sleep(24); return #time.sleep(85); return 
	
	if 'latitude' in ds_in.coords:
        	ds_in = ds_in.rename({'latitude': 'lat', 'longitude': 'lon'})  

	grid_out = xr.Dataset(
	{
		'lat': (['lat'], np.arange(-90+ddeg_out/2, 90, ddeg_out)),
		'lon': (['lon'], np.arange(0, 360, ddeg_out)),
	})
	regridder = xe.Regridder(ds_in, grid_out, algorithm, periodic = True )  
	ds_out = regridder(ds_in)
	ds_out.to_netcdf(out_file, compute = True)

status = "Invalid Year:"
if year != -1:
	regrid_func(in_dir, out_file, out_deg, algorithm, pressure_level)
	status = "Out file:"

end = datetime.datetime.now() 
diff = (end - start)	
seconds_in_day = 24 * 60 * 60
diff_m_s = divmod(diff.days * seconds_in_day + diff.seconds, 60)
time.sleep(10)
print(
  ( "[{0:<4s}{1:<3s}{2:>8s}] ".format(end.strftime("%a"), end.strftime("%d"), end.strftime("%X")) + ""
    "{0:>10s} {1:<4s} {2:>8s} {3:<80s}".format("End rank:", str(rank)+",", status, out_file)  + ""   
    "{0:<2s} ({1:>4s}m, {2:>2s}s)".format("", str(diff_m_s[0]), str(diff_m_s[1]) )  + "" ) 
)
