#!/bin/bash
# Help the MPI to run python script in separated conda envs.

. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/weatherbench

HOST=`hostname`
RANK=`cat $PBS_NODEFILE | uniq | grep -n $HOST | cut -f1 -d :` 
export NODE_RANK=$((RANK-1)) 
i=${NODE_RANK}
for j in {0..3}; do
    rank=$((${NODE_RANK}*4+j))
    printf 'rank: %-5d host: %s\n' "${rank}" "`hostname`"
    python "${REGRID_SCRIPT}" "${CONFIG_FILE}" ${rank}  &
done 
wait    