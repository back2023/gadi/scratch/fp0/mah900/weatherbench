#!/bin/bash

mkdir -p '/g/data/dk92/apps/NCI_weatherbench/2024.03.21/'
cp -r '/g/data/pp66/data/NCI-weatherbench/src/' '/g/data/dk92/apps/NCI_weatherbench/2024.03.21/'

mkdir -p '/g/data/dk92/apps/Modules/modulefiles/NCI_weatherbench/'
cp '/g/data/pp66/data/NCI-weatherbench/Modules/modulefiles/NCI_weatherbench/24.03.21' '/g/data/dk92/apps/Modules/modulefiles/NCI_weatherbench/2024.03.21'