# From the notebook: g/data/wb00/admin/testing/NCI_weatherbench/NCI_notebooks_v2/3b.cnn-example-1999.ipynb  
import pathlib
print ( pathlib.Path(__file__).parent.resolve() )

import sys
import warnings
import os
NOTEBOOK_PATH = "/g/data/wb00/admin/testing/NCI_weatherbench/NCI_notebooks_v3" 
warnings.filterwarnings('ignore')
os.environ["TF_ENABLE_ONEDNN_OPTS"] = "0"
#os.chdir(f'{NOTEBOOK_PATH}/scripts')
os.chdir(f'{NOTEBOOK_PATH}/scripts_model_3')
sys.path.insert(0, f'{NOTEBOOK_PATH}/src')
from score import *
import numpy as np
import xarray as xr
import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras.layers import Input, Dropout, Conv2D, Lambda, LeakyReLU
from tensorflow.python.client import device_lib
from dask.diagnostics import ProgressBar
from datetime import datetime
import train_nn
from dask.distributed import Client


def data_generate(ds, lead_time):
    ds_train = ds.sel(time=slice(*train_years))
    ds_valid = ds.sel(time=slice(*valid_years))
    ds_test  = ds.sel(time=slice(*test_years))

    print ("Data generation ... ")
    dic = {var: 500 for var in vars}
    dg_train = train_nn.DataGenerator(ds_train, dic, lead_time, batch_size=batch_size)
    dg_valid = train_nn.DataGenerator(ds_valid, dic, lead_time, batch_size=batch_size, mean=dg_train.mean,
                         std=dg_train.std, shuffle=False)
    dg_test =  train_nn.DataGenerator(ds_test, dic, lead_time, batch_size=batch_size, mean=dg_train.mean,
                         std=dg_train.std, shuffle=False)

    print(f'Mean = {dg_train.mean}; Std = {dg_train.std}')
    return dg_train, dg_valid, dg_test

#def train(dg_train, dg_valid, dg_test):
def train(dg_train, dg_valid, dg_test, input_shape, model_save_fn):
    print ("Train model ... ") 
    #model = train_nn.build_cnn(filters, kernels, input_shape=(32, 64, len(vars)), dr=dr)  
    model = train_nn.build_cnn(filters, kernels, input_shape=input_shape, dr=dr)  
    model.compile(keras.optimizers.Adam(lr), 'mse')
    print(model.summary())
    
    model.fit(dg_train, epochs=100, validation_data=dg_valid,
                          callbacks=[tf.keras.callbacks.EarlyStopping(
                          monitor='val_loss',
                          min_delta=0,
                          patience=patience,
                          verbose=1,
                          mode='auto'
                      )]
                      )
    print(f'Saving model weights: {model_save_fn}')
    model.save_weights(model_save_fn)   
    return model

def prediction(model, dg_test, iterative, iterative_lead_time, pred_save_fn):    
    print("Create predictions ...")
    pred = train_nn.create_iterative_predictions(model, dg_test, iterative_lead_time) if iterative \
                                                        else train_nn.create_predictions(model, dg_test)
    print(f'Saving predictions: {pred_save_fn}')
    pred.to_netcdf(pred_save_fn)
    return pred
    
def evaluate(pred, iterative, test_years):     
    print("Evaluating forecast ...")
    valid_years = list( range(int(test_years[0]), int(test_years[1])+1 ))
    print ('all test_years:', valid_years)
    z500_valid_files = [ file for year in valid_years for file in glob.glob (fr'{datadir}/geopotential/*{year}*') ] 
    t850_valid_files = [ file for year in valid_years for file in glob.glob (fr'{datadir}/temperature/*{year}*')  ]     

    z500_valid = train_nn.load_test_data(z500_valid_files, 'z', slice(*test_years)) 
    t850_valid = train_nn.load_test_data(t850_valid_files, 't', slice(*test_years))     
    valid      = xr.merge([z500_valid, t850_valid], compat='override').compute()
    
    print(train_nn.evaluate_iterative_forecast(pred, valid, compute_weighted_rmse).load() if iterative \
                                                else train_nn.compute_weighted_rmse(pred, valid).compute())

#def reset():
#    print("Resetting ...")
#    import IPython
#    IPython.Application.instance().kernel.do_shutdown(True)
#    IPython.Application.instance().kernel.do_shutdown(True)


if __name__ == '__main__':  
    
    start = datetime.now()
    train_nn.limit_mem()
    
    client = Client(n_workers=12, threads_per_worker=1)  
    
    print (60*"-")
    print( f'[{datetime.now().replace(microsecond=0)}]' )
    print (f"\nTF Device: {tf.config.list_physical_devices('GPU')}" )
    res     = '5.625'
    datadir = f'/g/data/wb00/NCI-Weatherbench/{res}deg' 
    print ('res:', res)
    print ("Data loading..." )

    year1 = int(sys.argv[1]) # 1999
    print(year1)
    year2 = 2022
    years = list(range(year1, year2+1))
    print (years)
    #exit('Test exit')
    z_files = [ file for year in years for file in glob.glob (fr'{datadir}/geopotential/*{year}*')  ] 
    t_files = [ file for year in years for file in glob.glob (fr'{datadir}/temperature/*{year}*')    ] 

    z = xr.open_mfdataset(z_files, combine='by_coords', parallel=True, chunks={'time': 10}).z.sel(level=[500]).load() 
    t = xr.open_mfdataset(t_files, combine='by_coords', parallel=True, chunks={'time': 10}).t.sel(level=[850]).drop('level').load() 
    datasets = [z, t]
    print ("Merging ... ")
    ds = xr.merge(datasets).compute()  

    print(ds['time'][:10])

    print(ds['time'][-10:])
    

    test_years =('2021', '2022')
    test_years = list (range(int (test_years[0]), int(test_years[1])+1 ))
    print ('test_years:', test_years)

    
    print (60*"-")
    #######################################################################
    #print ('24 hours (1 day)')

    #%%time
    print( f'[{datetime.now().replace(microsecond=0)}]' )

    print (60*"-")
    train_years=(str(year1), '2015')
    valid_years=('2016', '2020')
    test_years =('2021', '2022')
    batch_size = 32
    vars = ('z', 't')
    input_shape = (32, 64, len(vars))
    print ('input_shape:', input_shape)
    #lead_time = 24 # Set Lead time
    #print ('lead_time:', lead_time)
    
    #filters = [64, 64, 64, 64, 2]
    #kernels = [5, 5, 5, 5, 5]

    #filters = [64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 2]
    #kernels = [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5]
    #filters = [64, 128, 256, 512, 512, 512, 512, 512, 512, 512, 512, 512, 512, 256, 128, 64, 2]
    #kernels = [10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]
    
    kernels = [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5]
    filters = [64, 128, 256, 512, 512, 512, 512, 512, 512, 512, 512, 512, 512, 256, 128, 64, 2]    

    #print ('Model-3')
    #print ('filters:', filters)
    #print ('kernels:', kernels)
    
    #dr = 0
    #patience = 3
    save_prefix = f'{res}_model_3_{year1}_{year2}' 
    #print ('save_prefix:', save_prefix)
    #model_save_fn = f'/scratch/vp91/mah900/NCI-Weatherbench/pred_dir/saved_models/{save_prefix}_cnn_1d.h5'
    #pred_save_fn  = f'/scratch/vp91/mah900/NCI-Weatherbench/pred_dir/{save_prefix}_cnn_1d.nc'
    #lr = 1e-4
    #iterative = False
    #iterative_lead_time = None

    #%%time
    #print( f'[{datetime.now().replace(microsecond=0)}]' )
    #dg_train, dg_valid, dg_test = data_generate(ds, lead_time)
    #model = train(dg_train, dg_valid, dg_test, input_shape, model_save_fn)

    #%%time
    #print( f'[{datetime.now().replace(microsecond=0)}]' )
    #pred = prediction(model, dg_test, iterative, iterative_lead_time, pred_save_fn) 
    #evaluate(pred, iterative, test_years)    
    
    #print ('End 24 hours (1 day)')
    
    
    
    #print (60*"-")
    #######################################################################
    #print ('72 hours (3 days)')

    #%%time
    #print( f'[{datetime.now().replace(microsecond=0)}]' )

    #print (60*"-")
    #train_years=(str(year1), '2015')
    #valid_years=('2016', '2020')
    #test_years =('2021', '2022')
    #batch_size = 32
    #vars = ('z', 't')
    #input_shape = (32, 64, len(vars))
    #print ('input_shape:', input_shape)
    #lead_time = 72
    #print ('lead_time:', lead_time)
    
    #filters = [64, 64, 64, 64, 2]
    #kernels = [5, 5, 5, 5, 5]

    #filters = [64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 2]
    #kernels = [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5]
    #filters = [64, 128, 256, 512, 512, 512, 512, 512, 512, 512, 512, 512, 512, 256, 128, 64, 2]
    #kernels = [10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]

    #print ('Model-3')
    #print ('filters:', filters)
    #print ('kernels:', kernels)
    
    #dr = 0
    #patience = 3
    #save_prefix = f'{res}_model_1_{year1}_{year2}' 
    #print ('save_prefix:', save_prefix)
    #model_save_fn = f'/scratch/vp91/mah900/NCI-Weatherbench/pred_dir/saved_models/{save_prefix}_cnn_3d.h5'
    #pred_save_fn  = f'/scratch/vp91/mah900/NCI-Weatherbench/pred_dir/{save_prefix}_cnn_3d.nc'
    #lr = 1e-4
    #iterative = False
    #iterative_lead_time = None

    #%%time
    #print( f'[{datetime.now().replace(microsecond=0)}]' )
    #dg_train, dg_valid, dg_test = data_generate(ds, lead_time)
    #model = train(dg_train, dg_valid, dg_test, input_shape, model_save_fn)

    #%%time
    #print( f'[{datetime.now().replace(microsecond=0)}]' )
    #pred = prediction(model, dg_test, iterative, iterative_lead_time, pred_save_fn) 
    #evaluate(pred, iterative, test_years)   
    
    #print ('End 72 hours (3 days)')

    
    
    #print (60*"-")
    #######################################################################
    #print ('120 hours (5 days)')

    #%%time
    #print( f'[{datetime.now().replace(microsecond=0)}]' )

    #print (60*"-")
    #batch_size = 32
    #vars = ('z', 't')
    #lead_time = 120
    #print ('lead_time:', lead_time)
    #filters = [64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 2]
    #kernels = [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5]
    
    #print ('Model-3')
    #print ('filters:', filters)
    #print ('kernels:', kernels)
    
    #dr = 0
    #patience = 3
    #save_prefix = f'{res}_{year1}_{year2}' 
    #print ('save_prefix:', save_prefix)
    #model_save_fn= f'/scratch/vp91/mah900/NCI-Weatherbench/pred_dir/saved_models/{save_prefix}_cnn_5d.h5'
    #pred_save_fn = f'/scratch/vp91/mah900/NCI-Weatherbench/pred_dir/{save_prefix}_cnn_5d.nc'
    #lr = 1e-4
    #iterative = False
    #iterative_lead_time = None


    #%%time
    #print( f'[{datetime.now().replace(microsecond=0)}]' )
    #dg_train, dg_valid, dg_test =  data_generate(ds, lead_time)
    #model = train(dg_train, dg_valid, dg_test, input_shape, model_save_fn)


    #%%time
    #print( f'[{datetime.now().replace(microsecond=0)}]' )
    #pred = prediction(model, dg_test, iterative, iterative_lead_time, pred_save_fn) 
    #evaluate(pred, iterative, test_years)

    #print ('End 120 hours (5 days)')

    
    
    #print (60*"-")
    #######################################################################
    #print ('168 hours (7 days)')

    #%%time
    #print( f'[{datetime.now().replace(microsecond=0)}]' )

    #print (60*"-")
    #batch_size = 32
    #vars = ('z', 't')
    #lead_time = 168
    #print ('lead_time:', lead_time)
    #filters = [64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 2]
    #kernels = [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5]
    
    #print ('Model-3')
    #print ('filters:', filters)
    #print ('kernels:', kernels)
    
    #dr = 0
    #patience = 3
    #save_prefix = f'{res}_{year1}_{year2}' 
    #print ('save_prefix:', save_prefix)
    #model_save_fn= f'/scratch/vp91/mah900/NCI-Weatherbench/pred_dir/saved_models/{save_prefix}_cnn_7d.h5'
    #pred_save_fn = f'/scratch/vp91/mah900/NCI-Weatherbench/pred_dir/{save_prefix}_cnn_7d.nc'
    #lr = 1e-4
    #iterative = False
    #iterative_lead_time = None


    #%%time
    #print( f'[{datetime.now().replace(microsecond=0)}]' )
    #dg_train, dg_valid, dg_test =  data_generate(ds, lead_time)
    #model = train(dg_train, dg_valid, dg_test, input_shape, model_save_fn)


    #%%time
    #print( f'[{datetime.now().replace(microsecond=0)}]' )
    #pred = prediction(model, dg_test, iterative, iterative_lead_time, pred_save_fn) 
    #evaluate(pred, iterative, test_years)
    
    #print ('End 168 hours (7 days)')

    
    
    print (60*"-")
    #######################################################################
    print ('fccnn_6h_iter')

    #%%time
    print( f'[{datetime.now().replace(microsecond=0)}]' )

    print (60*"-")
    #batch_size = 32
    #vars = ('z', 't')
    lead_time = 6
    print ('lead_time:', lead_time)
    iterative_lead_time = 7 * 24 
    #filters = [64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 2]
    #kernels = [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5]
    #filters = [64, 128, 256, 512, 512, 512, 512, 512, 1024, 1024, 1024, 1024, 512, 512, 512, 512, 512, 256, 128, 64, 2]
    #kernels = [10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]
    #kernels = [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5]

    #filters = [64, 64, 64, 64, 2]
    #kernels = [5, 5, 5, 5, 5]
    
    print ('Model-3')
    print ('filters:', filters)
    print ('kernels:', kernels)    
    
    dr = 0
    patience = 3
    #save_prefix = f'{res}_{year1}_{year2}' 
    print ('save_prefix:', save_prefix)
    model_save_fn=f'/scratch/vp91/mah900/NCI-Weatherbench/pred_dir/saved_models/{save_prefix}_fccnn_6h_iter.h5'
    pred_save_fn =f'/scratch/vp91/mah900/NCI-Weatherbench/pred_dir/{save_prefix}_fccnn_6h_iter.nc'
    lr = 1e-4
    iterative = True

    #%%time
    print( f'[{datetime.now().replace(microsecond=0)}]' )
    dg_train, dg_valid, dg_test = data_generate(ds, lead_time)
    model = train(dg_train, dg_valid, dg_test, input_shape, model_save_fn)

    #%%time
    print( f'[{datetime.now().replace(microsecond=0)}]' )
    pred = prediction(model, dg_test, iterative, iterative_lead_time, pred_save_fn)  
    evaluate(pred, iterative, test_years)

    print ('End fccnn_6h_iter')

    
    
    print (60*"-")
    #######################################################################
    
    #print (pred)
    done = datetime.now()
    elapsed = done - start
    print(elapsed)