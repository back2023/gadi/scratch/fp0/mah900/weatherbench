import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import numpy as np
import xarray as xr
import xesmf as xe
import dask.array as da
import sys
import os
import glob
import time
import socket
import platform
import yaml
from dask.diagnostics import ProgressBar
import datetime
from dask.distributed import Client, progress

#client = Client(threads_per_worker=12, n_workers=1)

config_file_path = sys.argv[1]
rank = int(sys.argv[2])

config_file      = yaml.safe_load(open(config_file_path, "rb"))
ERA_param        = config_file.get("ERA_param")
out_deg          = config_file.get("out_deg")
pressure_level   = config_file.get("pressure_level")
in_dir_prefix    = config_file.get("in_dir_prefix")
out_file_prefix  = config_file.get("out_file_prefix")
algorithm        = config_file.get("algorithm")  
try:
	year         = config_file.get("year")[rank]
except IndexError:
	print ("Year index for rank NOT found, rank:", rank)
	year = -1

in_dir   = in_dir_prefix +"/"+str(year)+"/*"
out_file = out_file_prefix+"_"+str(year)+"_"+str(out_deg)+".nc"

time.sleep(5)
if rank != 0:
	time.sleep(3+rank*2)

if (rank==0):
	print("\u2554","\u2550"*120, "\u2557", sep="")
	print("\u2551 \u2554","\u2550"*116, "\u2557 \u2551", sep="")
	print("\u2551 \u2551{0:^116s}\u2551 \u2551".format("Common Param"))
	print("\u2551 \u2551{0:>20s} \u25B6 {1:<93s}\u2551 \u2551".format("ERA_param", ERA_param))
	print("\u2551 \u2551{0:>20s} \u25B6 {1:<93s}\u2551 \u2551".format("config_file_path", config_file_path))
	print("\u2551 \u2551{0:>20s} \u25B6 {1:<93f}\u2551 \u2551".format("out_deg", out_deg))
	print("\u2551 \u2551{0:>20s} \u25B6 {1:<93s}\u2551 \u2551".format("algorithm", algorithm))
	print("\u2551 \u2551{0:>20s} \u25B6 {1:<93s}\u2551 \u2551".format("pressure_level", str(pressure_level)))
	print("\u2551 \u2551{0:>20s} \u25B6 {1:<93s}\u2551 \u2551".format("in_dir_prefix", in_dir_prefix ))
	print("\u2551 \u2551{0:>20s} \u25B6 {1:<93s}\u2551 \u2551".format("out_file_prefix", out_file_prefix))
	print("\u2551 \u255A","\u2550"*116, "\u255D \u2551", sep="")
	print("\u255A","\u2550"*120, "\u255D", sep="")
	print("\n")
	
#x = datetime.datetime.now()
start = datetime.datetime.now()

print ("\u250C","\u2500"*120, "\u2510", sep="") 
print("\u250A{0:^120s}\u250A".format( start.strftime("%c") ))
print("\u250A{0:>20s} \u25BB {1:<97d}\u250A".format("rank", rank))
print("\u250A{0:>20s} \u25BB {1:<97s}\u250A".format("node", socket.gethostname()) )
print("\u250A{0:>20s} \u25BB {1:<97d}\u250A".format("year", year))
print("\u250A{0:>20s} \u25BB {1:<97s}\u250A".format("in_dir", in_dir))
print("\u250A{0:>20s} \u25BB {1:<97s}\u250A".format("out_file", out_file))
print ("\u2514","\u2500"*120, "\u2518", sep="")
if year != -1:
	inspect_data = xr.open_mfdataset(in_dir)  
	print (inspect_data)
	inspect_data.close()
else:
	print (" \u2590\u2590\u2590\u2590\u2590 {0:^108s} \u2590\u2590\u2590\u2590\u2590\n".format("Not a valid year: regrid function will not be called"))	
print ("\u2501"*122, sep="") 
print("\n")

if os.path.exists(out_file):
	print ("Exit: Destination file already exists:", out_file)
	exit(1)
 
def regrid_func (in_dir, out_file, ddeg_out, algorithm, p_level=None ):

	#data = xr.open_mfdataset(in_dir, chunks = {"time": 4}, parallel=False)
	#data = xr.open_mfdataset(in_dir, chunks = {"time": 1}, parallel=False)
	data = xr.open_mfdataset(in_dir, chunks = {"time":1, "level":1}, parallel=False)
	#data = xr.open_mfdataset(in_dir, chunks = {"time": 10}, parallel=False)
	#data = xr.open_mfdataset(in_dir, chunks = {"time": 5}, parallel=False)

	if p_level:
		ds_in = data.sel(level = p_level) 
	else: 
		ds_in = data
		
	#print ("Forced return (Testing only)\n"); time.sleep(85); return 
	
	if 'latitude' in ds_in.coords:
        	ds_in = ds_in.rename({'latitude': 'lat', 'longitude': 'lon'})  

	grid_out = xr.Dataset(
	{
		'lat': (['lat'], np.arange(-90+ddeg_out/2, 90, ddeg_out)),
		'lon': (['lon'], np.arange(0, 360, ddeg_out)),
	})
	regridder = xe.Regridder(ds_in, grid_out, algorithm, periodic=True )  
	ds_out = regridder(ds_in)
	ds_out.to_netcdf(out_file, compute=True)

status = "Invalid Year:"
if year != -1:
	regrid_func(in_dir, out_file, out_deg, algorithm, pressure_level)
	status = "Out file:"

#x = datetime.datetime.now() 
#print("[{0:<4s}{1:<3s}{2:>8s}] ".format(x.strftime("%a"), x.strftime("%d"), x.strftime("%X")), end="")
#print("{0:>10s} {1:<4s} {2:>12s} {3:<90s}\n".format("End rank:", str(rank)+",", status, out_file))
end = datetime.datetime.now() 
print("[{0:<4s}{1:<3s}{2:>8s}] ".format(end.strftime("%a"), end.strftime("%d"), end.strftime("%X")), end="")
print("{0:>10s} {1:<4s} {2:>8s} {3:<80s}".format("End rank:", str(rank)+",", status, out_file), end="")
diff = (end - start)	
seconds_in_day = 24 * 60 * 60
diff_m_s = divmod(diff.days * seconds_in_day + diff.seconds, 60)
print(" {0:<2s} ({1:>4s}m, {2:>2s}s)\n".format("", str(diff_m_s[0]), str(diff_m_s[1]) ) ) 

#if __name__ == "__main__":
#	client = Client(threads_per_worker=12, n_workers=1)