import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import numpy as np
import xarray as xr
import xesmf as xe
import dask.array as da

#nci_dir = '/g/data/rt52/era5/pressure-levels/reanalysis/t/1959/t_era5_oper_pl_19590101-19590131.nc'
nci_dir = '/g/data/rt52/era5/pressure-levels/reanalysis/t/1959/*'
data = xr.open_mfdataset(nci_dir, chunks={"time": 10})

#data = data.sel(time=slice('1959-01-01T01:00:00.000000000'))

data2 = data.sel(level=[ 50,  100,  150,  200,  250,  300,  400,  500,  600,  700,  850, 925, 1000])

ds_in = data2[["t", "time", "level", "latitude", "longitude" ]]

if 'latitude' in ds_in.coords:
        ds_in = ds_in.rename({'latitude': 'lat', 'longitude': 'lon'})  


ddeg_out =  5.625  # 2.8125 #  1.40625 # 5.625
grid_out = xr.Dataset(
    {
        'lat': (['lat'], np.arange(-90+ddeg_out/2, 90, ddeg_out)),
        'lon': (['lon'], np.arange(0, 360, ddeg_out)),
    })


regridder = xe.Regridder(
        ds_in, grid_out, method='bilinear', periodic=True ) # ,  reuse_weights=True )


ds_out = regridder(ds_in)

ds_out.to_netcdf("/g/data/wb00/admin/testing/weatherbench/nci_5.625/test.nc")
#result = ds_out.compute()

#result.to_netcdf("/g/data/wb00/admin/testing/weatherbench/nci_5.625/test.nc")

