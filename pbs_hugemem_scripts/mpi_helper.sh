#!/bin/bash
# Help the MPI to run python script in separated conda envs.

. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/weatherbench

HOST=`hostname`
RANK=`cat $PBS_NODEFILE | uniq | grep -n $HOST | cut -f1 -d :` 
export NODE_RANK=$((RANK-1)) 
#echo "HOST: ${HOST}"
#echo "NODE_RANK: ${NODE_RANK}"
sleep $((${NODE_RANK}*1))
#i=${NODE_RANK}
#for j in {0..3}; do
#for j in {0..39}; do
for j in {0..63}; do
#for j in {0..19}; do
#for j in {0..9}; do
#for j in {0..6}; do
    #echo -n "NODE_RANK: ${NODE_RANK} ->"
    rank=$((${NODE_RANK}*${WR_PER_NODE}+j))
    #echo "${rank}"
    #printf 'rank: %-5d host: %s  \n' "${rank}" "`hostname`"
    python "${REGRID_SCRIPT}" "${CONFIG_FILE}" ${rank}  &
    #printf 'rank: %-5d host: %s  ' "${rank}" "`hostname`"
    JOBS=$(jobs | wc -l)
    JOBS=$((${JOBS}-1))
    
    #echo -ne "  ( ${JOBS}: "
    if [ "${JOBS}" -ge "${WR_PER_NODE}" ]; then J_FLAG='o'
    #echo -e "\t\t\t\t\t\t\t ( Total jobs: ${JOBS}: over)"
    else   J_FLAG='u'
    #echo -e "\t\t\t\t\t\t\t ( Total jobs: ${JOBS}: Under )"    
    fi

    printf 'rank: %-5d host: %s ( local task: %-2d %s) \n' "${rank}" "`hostname`" "${JOBS}" "${J_FLAG}"
done 
wait    